package com.epam.esm.persistence.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class OrderInfo {

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(BigDecimal orderCost) {
        this.orderCost = orderCost;
    }

    public LocalDateTime getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(LocalDateTime purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    private BigDecimal orderCost;
    private LocalDateTime purchaseTime;
    public OrderInfo(Integer id, BigDecimal orderCost, LocalDateTime purchaseTime) {
        this.id = id;
        this.orderCost = orderCost;
        this.purchaseTime = purchaseTime;
    }
}
