package com.epam.esm.persistence.model;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static jakarta.persistence.GenerationType.IDENTITY;

@Entity(name = "Orders")
@Data
public class Order {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @MapsId
    private User user;

    @ManyToOne
    @JoinColumn(name = "certificate_id")
    private GiftCertificate giftCertificate;
    private BigDecimal orderCost;

    private LocalDateTime purchaseTime;

    @PrePersist
    public void prePersist() {
        purchaseTime = LocalDateTime.now();
    }

    public Integer getId() {
        return id;
    }

    public Order() {
    }

    public Order(Integer id, User user, GiftCertificate giftCertificate) {
        this.id = id;
        this.user = user;
        this.giftCertificate = giftCertificate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public GiftCertificate getGiftCertificate() {
        return giftCertificate;
    }

    public void setGiftCertificate(GiftCertificate giftCertificate) {
        this.giftCertificate = giftCertificate;
    }

    public BigDecimal getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(BigDecimal orderCost) {
        this.orderCost = orderCost;
    }

    public void setPurchaseTime(LocalDateTime purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public LocalDateTime getPurchaseTime() {
        return purchaseTime;
    }
    public OrderDTO toDTO() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(id);
        orderDTO.setOrderCost(orderCost);
        orderDTO.setPurchaseTime(purchaseTime);
        if (giftCertificate != null) {
            orderDTO.setGiftCertificateId(giftCertificate.getId());
        }
        return orderDTO;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", purchaseTime=" + purchaseTime +
                ", giftCertificate=" + (giftCertificate != null ? giftCertificate.getId() : null) +
                '}';
    }

}
