package com.epam.esm.persistence.repository;

import com.epam.esm.persistence.model.GiftCertificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GiftCertificateRepository extends JpaRepository<GiftCertificate, Long>, JpaSpecificationExecutor<GiftCertificate> {
    @Query(value = "SELECT DISTINCT gc FROM GiftCertificate gc JOIN gc.tags t WHERE t.name IN (:tags)")
    Page<GiftCertificate> findByTagsIn(@Param("tags") List<String> tags, Pageable pageable);
}
