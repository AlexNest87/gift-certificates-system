package com.epam.esm.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {

    private Integer id;
    private BigDecimal orderCost;
    private LocalDateTime purchaseTime;
    private Long giftCertificateId;
    private String giftCertificateName; // Add this property
    private String giftCertificateDescription; // Add this property
    private UserDTO user; // Add UserDTO property

    // Геттеры и сеттеры

    public static OrderDTO fromEntity(Order order) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(order.getId());
        orderDTO.setOrderCost(order.getOrderCost());
        orderDTO.setPurchaseTime(order.getPurchaseTime());

        // Set GiftCertificate information
        if (order.getGiftCertificate() != null) {
            orderDTO.setGiftCertificateId(order.getGiftCertificate().getId());
            orderDTO.setGiftCertificateName(order.getGiftCertificate().getName());
            orderDTO.setGiftCertificateDescription(order.getGiftCertificate().getDescription());
        }

        // Set UserDTO information
        orderDTO.setUser(UserDTO.fromEntity(order.getUser()));

        return orderDTO;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(BigDecimal orderCost) {
        this.orderCost = orderCost;
    }

    public LocalDateTime getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(LocalDateTime purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public Long getGiftCertificateId() {
        return giftCertificateId;
    }

    public void setGiftCertificateId(Long giftCertificateId) {
        this.giftCertificateId = giftCertificateId;
    }
}
