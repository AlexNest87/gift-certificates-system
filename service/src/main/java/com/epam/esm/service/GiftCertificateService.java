package com.epam.esm.service;

import com.epam.esm.persistence.model.GiftCertificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GiftCertificateService {

    GiftCertificate createGiftCertificate(GiftCertificate giftCertificate);

    public List<GiftCertificate> getAllGiftCertificates(String tagName, String partOfNameOrDescription, String sortBy, int page, int size);

    public List<GiftCertificate> getAllGiftCertificates(int page, int size);

    GiftCertificate getGiftCertificateById(Long id);

    GiftCertificate updateGiftCertificate(GiftCertificate giftCertificate);

    boolean deleteGiftCertificateById(Long id);
    Page<GiftCertificate> searchGiftCertificatesByTags(List<String> tags, Pageable pageable);
}
