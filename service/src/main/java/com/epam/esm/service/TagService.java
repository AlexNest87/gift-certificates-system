package com.epam.esm.service;

import com.epam.esm.persistence.model.Tag;

import java.util.List;

public interface TagService {
    public Tag createTag(Tag tag);

    public void deleteTag(Long id);

    public Tag getTagById(Long id);

    public Tag getTagByName(String name);

    public List<Tag> getAllTags(int page, int size);

    Tag updateTag(Tag tag);
}
