package com.epam.esm.service;

import com.epam.esm.persistence.model.GiftCertificate;
import com.epam.esm.persistence.model.Order;
import com.epam.esm.persistence.model.Tag;
import com.epam.esm.persistence.model.User;
import com.epam.esm.persistence.repository.GiftCertificateRepository;
import com.epam.esm.persistence.repository.OrderRepository;
import com.epam.esm.persistence.repository.UserRepository;
import com.epam.esm.service.exeption.OrderNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;
    private final GiftCertificateRepository certificateRepository;
    private final OrderRepository orderRepository;


    public UserServiceImpl(UserRepository userRepository, GiftCertificateRepository certificateRepository, OrderRepository orderRepository) {
        this.userRepository = userRepository;
        this.certificateRepository = certificateRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public Page<User> getAllUsers(Pageable pageable) {
        logger.info("Fetching all users");
        return userRepository.findAll(pageable);
    }

    @Override
    public User getUserById(Integer id) {
        logger.info("Fetching user by id: {}", id);
        Optional<User> optionalUser = userRepository.findById(id);
        User user = optionalUser.orElse(null);
        if (user == null) {
            logger.warn("User not found for id: {}", id);
        }
        return user;
    }

    @Override
    public Order purchaseGiftCertificate(Integer userId, Long certificateId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("User not found"));
        GiftCertificate certificate = certificateRepository.findById(certificateId).orElseThrow(() ->
                new EntityNotFoundException("Gift certificate not found"));

        Order order = new Order();
        order.setUser(user);
        order.setGiftCertificate(certificate);

        orderRepository.save(order);

        return order;
    }

    @Override
    public User createUser(User user) {
        logger.info("Creating user: {}", user);
        User createdUser = userRepository.save(user);
        logger.info("User created: {}", createdUser);
        return createdUser;
    }

    @Override
    public List<Order> getUserOrders(Integer userId, Pageable pageable) {
        logger.info("Fetching orders for user with ID: {}", userId);
        try {
            return orderRepository.findByUserId(userId, pageable);
        } catch (Exception e) {
            throw new OrderNotFoundException("Error occurred while fetching orders for user with ID: " + userId, e);
        }
    }

    @Override
    public Tag getMostUsedTagWithHighestCost(Integer userId, Pageable pageable) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            return null;
        }

        List<Order> userOrders = getUserOrders(userId, pageable);
        if (userOrders.isEmpty()) {
            return null;
        }

        Map<Tag, Integer> tagCountMap = new HashMap<>();
        BigDecimal highestCost = BigDecimal.ZERO;
        Tag mostUsedTag = null;

        for (Order order : userOrders) {
            BigDecimal orderCost = order.getGiftCertificate().getPrice();
            if (orderCost.compareTo(highestCost) > 0) {
                highestCost = orderCost;
                mostUsedTag = null;
            }

            Set<Tag> orderTags = order.getGiftCertificate().getTags();
            for (Tag tag : orderTags) {
                tagCountMap.put(tag, tagCountMap.getOrDefault(tag, 0) + 1);
                if (mostUsedTag == null || tagCountMap.get(tag) > tagCountMap.get(mostUsedTag)) {
                    mostUsedTag = tag;
                }
            }
        }

        return mostUsedTag;
    }

}
