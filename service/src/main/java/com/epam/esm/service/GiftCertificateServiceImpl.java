package com.epam.esm.service;

import com.epam.esm.persistence.model.GiftCertificate;
import com.epam.esm.persistence.model.Tag;
import com.epam.esm.persistence.repository.GiftCertificateRepository;
import com.epam.esm.persistence.repository.TagRepository;
import com.epam.esm.service.exeption.EntityNotFoundException;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Path;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class GiftCertificateServiceImpl implements GiftCertificateService {

    private static final Logger log = LoggerFactory.getLogger(GiftCertificateServiceImpl.class);

    private final GiftCertificateRepository certificateRepository;
    private final TagRepository tagRepository;

    public GiftCertificateServiceImpl(GiftCertificateRepository certificateRepository, TagRepository tagRepository) {
        this.certificateRepository = certificateRepository;
        this.tagRepository = tagRepository;
    }

    @Override
    public GiftCertificate getGiftCertificateById(Long id) {
        log.debug("Looking for a gift certificate with id {}", id);
        Optional<GiftCertificate> giftCertificate = certificateRepository.findById(id);
        if (giftCertificate.isPresent()) {
            log.info("Received a gift certificate with id {}", id);
            return giftCertificate.get();
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    @Transactional
    public GiftCertificate createGiftCertificate(GiftCertificate giftCertificate) {
        log.debug("Creating a new certificate");
        giftCertificate.getTags().removeIf(tag -> tagRepository.findByName(tag.getName()) != null);
        GiftCertificate createdCertificate = certificateRepository.save(giftCertificate);
        log.info("Created a new gift certificate with id {}", createdCertificate.getId());
        return createdCertificate;
    }

    @Override
    @Transactional
    public GiftCertificate updateGiftCertificate(GiftCertificate giftCertificate) {
        log.debug("Updating a gift certificate with id {}", giftCertificate.getId());
        GiftCertificate existingCertificate = certificateRepository.findById(giftCertificate.getId())
                .orElseThrow(() -> new EntityNotFoundException("GiftCertificate not found"));

        // Update the fields of the existing GiftCertificate
        updateFields(existingCertificate, giftCertificate);

        existingCertificate.getTags().addAll(getUniqueTags(giftCertificate));

        // Save the updated GiftCertificate
        return certificateRepository.save(existingCertificate);
    }


    @Override
    public boolean deleteGiftCertificateById(Long id) {
        log.debug("Deleting gift certificate with id {}", id);
        Optional<GiftCertificate> certificateOptional = certificateRepository.findById(id);
        if (certificateOptional.isPresent()) {
            certificateRepository.deleteById(id);
            log.info("Gift certificate with id {} is deleted", certificateOptional.get().getId());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<GiftCertificate> getAllGiftCertificates(String tagName, String partOfNameOrDescription, String sortBy, int page, int size) {
        log.debug("Retrieving gift certificates");
        Specification<GiftCertificate> specification = Specification.where(null);

        if (tagName != null) {
            specification = specification.and(hasTag(tagName));
        }

        if (partOfNameOrDescription != null) {
            specification = specification.and(containsNameOrDescription(partOfNameOrDescription));
        }

        if (sortBy != null) {
            String[] sortParams = sortBy.split(",");
            for (String sortParam : sortParams) {
                String[] sortParts = sortParam.split(":");
                String sortField = sortParts[0];
                Sort.Direction sortDirection = sortParts.length > 1 && sortParts[1].equalsIgnoreCase("desc") ?
                        Sort.Direction.DESC : Sort.Direction.ASC;
                specification = specification.and(orderBy(sortField, sortDirection));
            }
        }

        Pageable pageable = PageRequest.of(page, size);
        Page<GiftCertificate> pageResult = certificateRepository.findAll(specification, pageable);
        return pageResult.getContent();
    }

    private Specification<GiftCertificate> hasTag(String tagName) {
        return (root, query, builder) -> {
            Join<GiftCertificate, Tag> join = root.join("tags", JoinType.LEFT);
            return builder.equal(join.get("name"), tagName);
        };
    }

    private Specification<GiftCertificate> containsNameOrDescription(String partOfNameOrDescription) {
        return (root, query, builder) -> builder.or(
                builder.like(root.get("name"), "%" + partOfNameOrDescription + "%"),
                builder.like(root.get("description"), "%" + partOfNameOrDescription + "%")
        );
    }

    private Specification<GiftCertificate> orderBy(String sortField, Sort.Direction sortDirection) {
        return (root, query, builder) -> {
            Path<Object> fieldPath = root.get(sortField);
            query.orderBy(sortDirection == Sort.Direction.ASC ? builder.asc(fieldPath) : builder.desc(fieldPath));
            return null;
        };
    }


    @Override
    public List<GiftCertificate> getAllGiftCertificates(int  page, int size) {
        log.debug("Retrieving gift certificates");
        Pageable pageable = PageRequest.of(page, size);
        Page<GiftCertificate> certificatePage = certificateRepository.findAll(pageable);
        return certificatePage.getContent();
    }


    private Set<Tag> getUniqueTags(GiftCertificate giftCertificate) {

        Set<Tag> uniqueTags = new HashSet<>();

        for (Tag tag : giftCertificate.getTags()) {
            Tag existingTag = tagRepository.findByName(tag.getName());

            if (existingTag != null) {
                // Tag with the same name already exists, so use the existing tag
                uniqueTags.add(existingTag);
            } else {
                // Save the new tag and add it to the set of unique tags
                Tag savedTag = tagRepository.save(tag);
                uniqueTags.add(savedTag);
            }
        }

        return uniqueTags;
    }

    @Override
    public Page<GiftCertificate> searchGiftCertificatesByTags(List<String> tags, Pageable pageable) {
        return certificateRepository.findByTagsIn(tags, pageable);
    }

    /**
     * Updates the fields in an existing GiftCertificate entity with values from a new GiftCertificate entity.
     * Only fields that are not null in the new entity are updated.
     *
     * @param existingCertificate The existing GiftCertificate entity to update
     * @param newCertificate      The new GiftCertificate entity with updated field values
     */
    private void updateFields(GiftCertificate existingCertificate, GiftCertificate newCertificate) {
        if (newCertificate.getName() != null) {
            existingCertificate.setName(newCertificate.getName());
        }
        if (newCertificate.getDescription() != null) {
            existingCertificate.setDescription(newCertificate.getDescription());
        }
        if (newCertificate.getPrice() != null) {
            existingCertificate.setPrice(newCertificate.getPrice());
        }
        if (newCertificate.getDuration() != null) {
            existingCertificate.setDuration(newCertificate.getDuration());
        }
        if (newCertificate.getCreateDate() != null) {
            existingCertificate.setCreateDate(newCertificate.getCreateDate());
        }
        if (newCertificate.getLastUpdateDate() != null) {
            existingCertificate.setLastUpdateDate(newCertificate.getLastUpdateDate());
        }
    }


}