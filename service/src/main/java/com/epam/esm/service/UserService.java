package com.epam.esm.service;

import com.epam.esm.persistence.model.Order;
import com.epam.esm.persistence.model.Tag;
import com.epam.esm.persistence.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    Page<User> getAllUsers(Pageable pageable);

    User getUserById(Integer id);

    Order purchaseGiftCertificate(Integer userId, Long certificateId);

    User createUser(User user);

    List<Order> getUserOrders(Integer userId, Pageable pageable);

    Tag getMostUsedTagWithHighestCost(Integer userId, Pageable pageable);
}
