package com.epam.esm.service;

import com.epam.esm.persistence.model.GiftCertificate;
import com.epam.esm.persistence.model.Order;
import com.epam.esm.persistence.model.Tag;
import com.epam.esm.persistence.model.User;
import com.epam.esm.persistence.repository.GiftCertificateRepository;
import com.epam.esm.persistence.repository.OrderRepository;
import com.epam.esm.persistence.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private GiftCertificateRepository certificateRepository;
    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    void testGetAllUsers() {
        List<User> userList = Arrays.asList(
                new User(1, "John"),
                new User(2, "Jane")
        );
        Pageable pageable = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(userList, pageable, userList.size());
        when(userRepository.findAll(pageable)).thenReturn(userPage);

        Page<User> result = userService.getAllUsers(pageable);

        assertEquals(userPage, result);
        verify(userRepository, times(1)).findAll(pageable);
    }

    @Test
    void testGetUserById_UserExists() {
        Integer userId = 1;
        User user = new User(userId, "John");
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        User result = userService.getUserById(userId);

        assertEquals(user, result);
        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    void testGetUserById_UserDoesNotExist() {
        Integer userId = 1;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        User result = userService.getUserById(userId);

        assertNull(result);
        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    void testPurchaseGiftCertificate() {
        User user = new User(1, "John");
        GiftCertificate certificate = new GiftCertificate(1L, "Gift Certificate", "Description", BigDecimal.valueOf(10), 7);

        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        when(certificateRepository.findById(1L)).thenReturn(Optional.of(certificate));

        Order order = userService.purchaseGiftCertificate(1, 1L);

        verify(userRepository).findById(1);
        verify(certificateRepository).findById(1L);

        verify(orderRepository).save(order);

        assertEquals(user, order.getUser());
        assertEquals(certificate, order.getGiftCertificate());
    }

    @Test
    void testPurchaseGiftCertificate_UserNotFound() {
        when(userRepository.findById(1)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.purchaseGiftCertificate(1, 1L));

        verify(userRepository).findById(1);
    }

    @Test
    void testPurchaseGiftCertificate_GiftCertificateNotFound() {
        User user = new User(1, "John");

        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        when(certificateRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.purchaseGiftCertificate(1, 1L));

        verify(userRepository).findById(1);
        verify(certificateRepository).findById(1L);
    }

    @Test
    void testCreateUser() {
        User user = new User();
        user.setId(1);

        when(userRepository.save(user)).thenReturn(user);

        User createdUser = userService.createUser(user);

        verify(userRepository, times(1)).save(user);
        assertEquals(user, createdUser);
    }


    @Test
    void testFindByUserId() {
        Integer userId = 1;
        Pageable pageable = PageRequest.of(0, 10);

        List<Order> orders = new ArrayList<>();
        orders.add(new Order(1, new User(userId), new GiftCertificate(1L, "Order 1", "GiftCertificate description")));
        orders.add(new Order(2, new User(userId), new GiftCertificate(2L, "Order 2", "GiftCertificate description")));

        when(orderRepository.findByUserId(userId, pageable)).thenReturn(orders);

        List<Order> result = orderRepository.findByUserId(userId, pageable);

        verify(orderRepository, times(1)).findByUserId(userId, pageable);
        assertEquals(orders, result);
    }

//    @Test
    public void testGetMostUsedTagWithHighestCost() {
        User user = new User();
        user.setId(1);

        GiftCertificate giftCertificate1 = new GiftCertificate();
        giftCertificate1.setPrice(new BigDecimal("50.00"));

        GiftCertificate giftCertificate2 = new GiftCertificate();
        giftCertificate2.setPrice(new BigDecimal("100.00"));

        Tag tag1 = new Tag();
        tag1.setId(1L);

        Tag tag2 = new Tag();
        tag2.setId(2L);

        Order order1 = new Order();
        order1.setGiftCertificate(giftCertificate1);
        order1.setPurchaseTime(LocalDateTime.now());
        order1.getGiftCertificate().getTags().add(tag1);

        Order order2 = new Order();
        order2.setGiftCertificate(giftCertificate2);
        order2.setPurchaseTime(LocalDateTime.now());
        order2.getGiftCertificate().getTags().add(tag1);
        order2.getGiftCertificate().getTags().add(tag2);

        List<Order> userOrders = Arrays.asList(order1, order2);

        UserRepository userRepository = mock(UserRepository.class);
        when(userRepository.findById(1)).thenReturn(Optional.of(user));

        UserService userService = new UserServiceImpl(userRepository, null, orderRepository);
        Pageable pageable = PageRequest.of(0, 10);
        Tag result = userService.getMostUsedTagWithHighestCost(1, pageable);

        assertNotNull(result);
        assertEquals(tag1.getId(), result.getId());
    }

}
