package com.epam.esm.service;

import com.epam.esm.persistence.model.Tag;
import com.epam.esm.persistence.repository.TagRepository;
import com.epam.esm.service.exeption.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class TagServiceImplTest {

    @Mock
    private TagRepository tagRepository;

    @InjectMocks
    private TagServiceImpl tagService;

    @Test
    void createTag_ShouldCreateTag_WhenValidTagPassed() {
        Tag tag = new Tag("Test Tag");
        when(tagRepository.save(tag)).thenReturn(tag);

        Tag createdTag = tagService.createTag(tag);
        assertNotNull(createdTag);
        assertEquals(tag.getName(), createdTag.getName());
    }

    @Test
    void deleteTag_ShouldDeleteTag_WhenValidTagIdPassed() {
        Long tagId = 1L;
        when(tagRepository.findById(tagId)).thenReturn(Optional.of(new Tag(tagId, "Test Tag")));

        tagService.deleteTag(tagId);

        verify(tagRepository, times(1)).deleteById(tagId);
    }

    @Test
    void deleteTag_ShouldThrowResourceNotFoundException_WhenInvalidTagIdPassed() {
        Long tagId = 1L;
        when(tagRepository.findById(tagId)).thenReturn(Optional.ofNullable(null));

        assertThrows(EntityNotFoundException.class, () -> tagService.deleteTag(tagId));
    }

    @Test
    void getTagById_ShouldReturnTag_WhenValidTagIdPassed() {
        Long tagId = 1L;
        Tag tag = new Tag(tagId, "Test Tag");
        when(tagRepository.findById(tagId)).thenReturn(Optional.of(tag));

        Tag returnedTag = tagService.getTagById(tagId);
        assertNotNull(returnedTag);
        assertEquals(tag.getId(), returnedTag.getId());
        assertEquals(tag.getName(), returnedTag.getName());
    }

    @Test
    void getTagByName_ShouldReturnOptionalTag_WhenValidTagNamePassed() {
        String tagName = "Test Tag";
        Tag tag = new Tag(1L, tagName);
        when(tagRepository.findByName(tagName)).thenReturn(tag);

        Tag serviceTag = tagService.getTagByName(tagName);

        assertNotNull(serviceTag);
        assertEquals(tag.getId(), serviceTag.getId());
        assertEquals(tag.getName(), serviceTag.getName());
    }

    @Test
    void getAllTags_ShouldReturnListOfTags_WhenCalled() {
        List<Tag> tags = Arrays.asList(new Tag(1L, "Test Tag 1"), new Tag(2L, "Test Tag 2"));
        Page<Tag> tagPage = new PageImpl<>(tags);
        when(tagRepository.findAll(Mockito.any(Pageable.class))).thenReturn(tagPage);

        List<Tag> returnedTags = tagService.getAllTags(0, 10);

        assertNotNull(returnedTags);
        assertEquals(tags.size(), returnedTags.size());
        for (int i = 0; i < tags.size(); i++) {
            assertEquals(tags.get(i).getId(), returnedTags.get(i).getId());
            assertEquals(tags.get(i).getName(), returnedTags.get(i).getName());
        }

        Mockito.verify(tagRepository, Mockito.times(1)).findAll(Mockito.any(Pageable.class));
    }

    @Test
    void updateTag_ShouldUpdateTag_WhenValidTagPassed() {
        Tag tag = new Tag(1L, "Test Tag");
        when(tagRepository.save(tag)).thenReturn(tag);

        Tag updatedTag = tagService.updateTag(tag);

        assertNotNull(updatedTag);
        assertEquals(tag.getId(), updatedTag.getId());
        assertEquals(tag.getName(), updatedTag.getName());
    }
}