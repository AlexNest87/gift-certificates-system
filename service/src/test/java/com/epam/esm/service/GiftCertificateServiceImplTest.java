package com.epam.esm.service;

import com.epam.esm.persistence.model.GiftCertificate;
import com.epam.esm.persistence.model.Tag;
import com.epam.esm.persistence.repository.GiftCertificateRepository;
import com.epam.esm.persistence.repository.TagRepository;
import com.epam.esm.service.exeption.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class GiftCertificateServiceImplTest {

    @Mock
    GiftCertificateRepository giftCertificateRepository;
    @Mock
    TagRepository tagRepository;
    @InjectMocks
    private GiftCertificateServiceImpl giftCertificateService;

    @Test
    void updateGiftCertificate_existingCertificate_shouldUpdateAndReturnCertificate() {
        long certificateId = 1L;
        GiftCertificate existingCertificate = new GiftCertificate();
        existingCertificate.setId(certificateId);
        existingCertificate.setName("Old Certificate");
        existingCertificate.setDescription("Old Description");
        existingCertificate.setPrice(BigDecimal.valueOf(10.00));
        existingCertificate.setDuration(5);
        existingCertificate.setTags(new HashSet<>(Arrays.asList(new Tag("tag1"), new Tag("tag2"))));

        when(giftCertificateRepository.findById(certificateId)).thenReturn(Optional.of(existingCertificate));
        when(giftCertificateRepository.save(existingCertificate)).thenReturn(existingCertificate);

        GiftCertificate newCertificate = new GiftCertificate();
        newCertificate.setId(certificateId);
        newCertificate.setName("New Certificate");
        newCertificate.setDescription("New Description");
        newCertificate.setPrice(BigDecimal.valueOf(20.00));
        newCertificate.setDuration(10);
        newCertificate.setTags(new HashSet<>(Arrays.asList(new Tag("tag1"), new Tag("tag3"))));

        GiftCertificate updatedCertificate = giftCertificateService.updateGiftCertificate(newCertificate);

        verify(giftCertificateRepository).save(existingCertificate);
        assertEquals(newCertificate.getName(), updatedCertificate.getName());
        assertEquals(newCertificate.getDescription(), updatedCertificate.getDescription());
        assertEquals(newCertificate.getPrice(), updatedCertificate.getPrice());
        assertEquals(newCertificate.getDuration(), updatedCertificate.getDuration());
    }


    @Test
    void updateGiftCertificate_nonExistingCertificate_shouldThrowException() {
        long certificateId = 1L;
        GiftCertificate newCertificate = new GiftCertificate();
        newCertificate.setId(certificateId);
        when(giftCertificateRepository.findById(certificateId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () ->
                giftCertificateService.updateGiftCertificate(newCertificate)
        );
    }

    @Test
    void testGetGiftCertificates() {
        List<GiftCertificate> giftCertificates = new ArrayList<>();
        giftCertificates.add(new GiftCertificate(1L, "Certificate 1", "Description 1"));
        giftCertificates.add(new GiftCertificate(2L, "Certificate 2", "Description 2"));

        Page<GiftCertificate> page = new PageImpl<>(giftCertificates);
        Pageable pageable = PageRequest.of(0, 10);
        when(giftCertificateRepository.findAll(pageable)).thenReturn(page);

        List<GiftCertificate> result = giftCertificateService.getAllGiftCertificates(0, 10);

        assertEquals(2, result.size());
        assertEquals("Certificate 1", result.get(0).getName());
        assertEquals("Description 2", result.get(1).getDescription());

        verify(giftCertificateRepository, times(1)).findAll(pageable);
    }



    @Test
    void testGetGiftCertificateById() {
        Long id = 1L;
        GiftCertificate expectedGiftCertificate = new GiftCertificate(id, "Test Certificate", "Test Description", BigDecimal.TEN, 5);
        Optional<GiftCertificate> optionalGiftCertificate = Optional.of(expectedGiftCertificate);
        when(giftCertificateRepository.findById(id)).thenReturn(optionalGiftCertificate);

        GiftCertificate resultGiftCertificate = giftCertificateService.getGiftCertificateById(id);

        verify(giftCertificateRepository, times(1)).findById(id);
        assertEquals(expectedGiftCertificate, resultGiftCertificate);
    }

    @Test
    void testGetGiftCertificateById_ResourceNotFoundException() {
        Long id = 1L;
        when(giftCertificateRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> giftCertificateService.getGiftCertificateById(id));
    }

    @Test
    void testCreateGiftCertificate() {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("Test certificate");
        giftCertificate.setDescription("Test certificate description");
        giftCertificate.setPrice(BigDecimal.valueOf(10.0));
        giftCertificate.setDuration(10);
        giftCertificate.setCreateDate(LocalDateTime.now());
        giftCertificate.setLastUpdateDate(LocalDateTime.now());
        giftCertificate.setTags(new HashSet<>(List.of(new Tag("Tag1"), new Tag("Tag10"))));

        when(giftCertificateRepository.save(ArgumentMatchers.any(GiftCertificate.class)))
                .thenReturn(giftCertificate);

        GiftCertificate result = giftCertificateService.createGiftCertificate(giftCertificate);

        assertEquals(giftCertificate, result);
    }

    @Test
    void deleteGiftCertificateById_Should_CallDaoDelete_When_GiftCertificateExists() {
        Long id = 1L;
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setId(id);
        Optional<GiftCertificate> certificateOptional = Optional.of(giftCertificate);
        when(giftCertificateRepository.findById(id)).thenReturn(certificateOptional);

        boolean result = giftCertificateService.deleteGiftCertificateById(id);

        assertTrue(result);
        verify(giftCertificateRepository, times(1)).deleteById(id);
        verify(giftCertificateRepository, times(1)).findById(id);

    }

    @Test
    void deleteGiftCertificateById_Should_ReturnFalse_When_CertificateDoesNotExist() {
        Long id = 1L;
        Optional<GiftCertificate> certificateOptional = Optional.empty();
        when(giftCertificateRepository.findById(id)).thenReturn(certificateOptional);

        boolean result = giftCertificateService.deleteGiftCertificateById(id);

        assertFalse(result);
        verify(giftCertificateRepository, never()).deleteById(id);
        verify(giftCertificateRepository, times(1)).findById(id);
    }

}