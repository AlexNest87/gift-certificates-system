package com.epam.esm.api.controller;

import com.epam.esm.persistence.model.*;
import com.epam.esm.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public ResponseEntity<CollectionModel<EntityModel<UserDTO>>> getAllUsers(@RequestParam(defaultValue = "0") int page,
                                                                             @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> userPage = userService.getAllUsers(pageable);
        List<User> users = userPage.getContent();

        List<EntityModel<UserDTO>> entityModels = users.stream()
                .map(user -> EntityModel.of(user.toDTO(),
                        linkTo(methodOn(UserController.class).getUserById(user.getId())).withSelfRel(),
                        linkTo(methodOn(UserController.class).getAllUsers(page, size)).withRel("users")))
                .toList();

        CollectionModel<EntityModel<UserDTO>> collectionModel = CollectionModel.of(entityModels,
                linkTo(methodOn(UserController.class).getAllUsers(page, size)).withSelfRel());

        return ResponseEntity.ok(collectionModel);
    }


    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<UserDTO>> getUserById(@PathVariable Integer id) {
        User user = userService.getUserById(id);
        if (user != null) {
            UserDTO userDTO = user.toDTO(); // Use toDTO() method here
            EntityModel<UserDTO> userEntityModel = EntityModel.of(userDTO,
                    linkTo(methodOn(UserController.class).getUserById(id)).withSelfRel(),
                    linkTo(methodOn(UserController.class).getAllUsers(0, 10)).withRel("users"));
            return ResponseEntity.ok(userEntityModel);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping("{userId}/orders/{certificateId}")
    public ResponseEntity<EntityModel<Order>> purchaseGiftCertificate(
            @PathVariable("userId") Integer userId, @PathVariable("certificateId") Long certificateId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            return ResponseEntity.status(NOT_FOUND).build();
        }

        try {
            Order order = userService.purchaseGiftCertificate(userId, certificateId);
            return ResponseEntity.status(HttpStatus.CREATED).body(EntityModel.of(order));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<EntityModel<User>> createUser(@RequestBody User user) {
        User createdUser = userService.createUser(user);
        EntityModel<User> userEntityModel = EntityModel.of(createdUser);
        return ResponseEntity.status(HttpStatus.CREATED).body(userEntityModel);
    }

    @GetMapping("/{userId}/orders")
    public List<OrderDTO> getUserOrders(@PathVariable Integer userId,
                                        @RequestParam(defaultValue = "0") int page,
                                        @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Order> orders = userService.getUserOrders(userId, pageable);
        return orders.stream()
                .map(OrderDTO::fromEntity)
                .toList();
    }


    @GetMapping("/{userId}/order-info")
    public ResponseEntity<List<EntityModel<OrderInfo>>> getUserOrderInfo(@PathVariable("userId") Integer userId,
                                                                         @RequestParam(defaultValue = "0") int page,
                                                                         @RequestParam(defaultValue = "10") int size) {
        User user = userService.getUserById(userId);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Pageable pageable = PageRequest.of(page, size);
        List<Order> orders = userService.getUserOrders(userId, pageable);

        List<OrderInfo> orderInfoList = new ArrayList<>();
        for (Order order : orders) {
            BigDecimal orderCost = order.getGiftCertificate().getPrice();
            LocalDateTime purchaseTime = order.getPurchaseTime();

            OrderInfo orderInfo = new OrderInfo(order.getId(), orderCost, purchaseTime);
            orderInfoList.add(orderInfo);
        }

        if (orderInfoList.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        List<EntityModel<OrderInfo>> orderInfoModels = orderInfoList.stream()
                .map(this::createOrderInfoEntityModel)
                .toList();

        return ResponseEntity.ok(orderInfoModels);
    }

    private EntityModel<OrderInfo> createOrderInfoEntityModel(OrderInfo orderInfo) {
        EntityModel<OrderInfo> orderInfoModel = EntityModel.of(orderInfo);
        orderInfoModel.add(linkTo(methodOn(UserController.class).getUserOrderInfo(orderInfo.getId(), 0, 10)).withSelfRel());
        orderInfoModel.add(linkTo(methodOn(UserController.class).getUserById(orderInfo.getId())).withRel("user"));

        return orderInfoModel;
    }

    @GetMapping("/{userId}/most-used-tag")
    public ResponseEntity<EntityModel<Tag>> getMostUsedTagWithHighestCost(@PathVariable("userId") Integer userId,
                                                                          @RequestParam(defaultValue = "0") int page,
                                                                          @RequestParam(defaultValue = "10") int size) {
        User user = userService.getUserById(userId);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Pageable pageable = PageRequest.of(page, size);
        Tag mostUsedTag = userService.getMostUsedTagWithHighestCost(userId, pageable);

        if (mostUsedTag == null) {
            return ResponseEntity.notFound().build();
        }

        EntityModel<Tag> tagModel = EntityModel.of(mostUsedTag);
        tagModel.add(linkTo(methodOn(UserController.class).getMostUsedTagWithHighestCost(userId, 0, 10)).withSelfRel());
        tagModel.add(linkTo(methodOn(TagController.class).getTag(mostUsedTag.getId())).withRel("tag-details"));

        return ResponseEntity.ok(tagModel);
    }

}
