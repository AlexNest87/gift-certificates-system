package com.epam.esm.api.controller;

import com.epam.esm.persistence.model.GiftCertificate;
import com.epam.esm.service.GiftCertificateService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/gift-certificates")
public class GiftCertificateController {

    private static final String DELETE = "delete";
    private static final String CREATE = "create";
    private static final String UPDATE = "update";

    private final GiftCertificateService giftCertificateService;

    public GiftCertificateController(GiftCertificateService giftCertificateService) {
        this.giftCertificateService = giftCertificateService;
    }

    @PostMapping
    public ResponseEntity<EntityModel<GiftCertificate>> createGiftCertificate(@RequestBody GiftCertificate giftCertificate) {
        GiftCertificate createdGiftCertificate = giftCertificateService.createGiftCertificate(giftCertificate);
        EntityModel<GiftCertificate> resource = EntityModel.of(createdGiftCertificate);
        URI location = linkTo(methodOn(GiftCertificateController.class).getGiftCertificate(createdGiftCertificate.getId())).toUri();
        resource.add(linkTo(methodOn(GiftCertificateController.class).createGiftCertificate(null)).withRel(CREATE));
        resource.add(linkTo(methodOn(GiftCertificateController.class).updateGiftCertificate(createdGiftCertificate.getId(), null)).withRel(UPDATE));
        resource.add(linkTo(methodOn(GiftCertificateController.class).deleteGiftCertificate(createdGiftCertificate.getId())).withRel(DELETE));
        return ResponseEntity.created(location).body(resource);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<GiftCertificate>> getGiftCertificate(@PathVariable Long id) {
        GiftCertificate giftCertificate = giftCertificateService.getGiftCertificateById(id);
        EntityModel<GiftCertificate> resource = EntityModel.of(giftCertificate);
        resource.add(linkTo(methodOn(GiftCertificateController.class).getGiftCertificate(id)).withSelfRel());
        resource.add(linkTo(methodOn(GiftCertificateController.class).createGiftCertificate(null)).withRel(CREATE));
        resource.add(linkTo(methodOn(GiftCertificateController.class).updateGiftCertificate(id, null)).withRel(UPDATE));
        resource.add(linkTo(methodOn(GiftCertificateController.class).deleteGiftCertificate(id)).withRel(DELETE));
        return ResponseEntity.ok(resource);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EntityModel<GiftCertificate>> updateGiftCertificate(@PathVariable Long id, @RequestBody GiftCertificate giftCertificate) {
        giftCertificate.setId(id);
        GiftCertificate updatedGiftCertificate = giftCertificateService.updateGiftCertificate(giftCertificate);
        EntityModel<GiftCertificate> resource = EntityModel.of(updatedGiftCertificate);
        resource.add(linkTo(methodOn(GiftCertificateController.class).getGiftCertificate(id)).withSelfRel());
        resource.add(linkTo(methodOn(GiftCertificateController.class).createGiftCertificate(null)).withRel(CREATE));
        resource.add(linkTo(methodOn(GiftCertificateController.class).updateGiftCertificate(id, null)).withRel(UPDATE));
        resource.add(linkTo(methodOn(GiftCertificateController.class).deleteGiftCertificate(id)).withRel(DELETE));
        return ResponseEntity.ok(resource);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteGiftCertificate(@PathVariable Long id) {
        boolean deleted = giftCertificateService.deleteGiftCertificateById(id);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            String message = "Gift certificate with ID " + id + " not found.";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<CollectionModel<EntityModel<GiftCertificate>>> getAllGiftCertificates(@RequestParam(defaultValue = "0") int page,
                                                                                                @RequestParam(defaultValue = "100") int size) {
        List<GiftCertificate> certificates = giftCertificateService.getAllGiftCertificates(page, size);
        List<EntityModel<GiftCertificate>> entityModels = certificates.stream()
                .map(this::createGiftCertificateEntityModel)
                .toList();

        CollectionModel<EntityModel<GiftCertificate>> collectionModel = CollectionModel.of(entityModels);
        collectionModel.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).getAllGiftCertificates(page, size)).withSelfRel());

        return ResponseEntity.ok(collectionModel);
    }


    @GetMapping("/sorted")
    public ResponseEntity<CollectionModel<EntityModel<GiftCertificate>>> getAllGiftCertificates(
            @RequestParam(name = "tag", required = false) String tagName,
            @RequestParam(name = "partOfNameOrDescription", required = false) String partOfNameOrDescription,
            @RequestParam(name = "sortBy", required = false) String sortBy,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {

        List<GiftCertificate> giftCertificates = giftCertificateService.getAllGiftCertificates(tagName, partOfNameOrDescription, sortBy, page, size);

        if (giftCertificates.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            List<EntityModel<GiftCertificate>> entityModels = giftCertificates.stream()
                    .map(this::createGiftCertificateEntityModel)
                    .toList();

            CollectionModel<EntityModel<GiftCertificate>> collectionModel = CollectionModel.of(entityModels);
            collectionModel.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).getAllGiftCertificates(tagName, partOfNameOrDescription, sortBy, page, size)).withSelfRel());

            return ResponseEntity.ok(collectionModel);
        }
    }

    @GetMapping("/search-by-tags")
    public ResponseEntity<PagedModel<EntityModel<GiftCertificate>>> searchGiftCertificatesByTags(
            @RequestParam("tags") List<String> tags,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {

        Pageable pageable = PageRequest.of(page, size);
        Page<GiftCertificate> giftCertificatePage = giftCertificateService.searchGiftCertificatesByTags(tags, pageable);

        List<EntityModel<GiftCertificate>> entityModels = giftCertificatePage.getContent().stream()
                .map(this::createGiftCertificateEntityModel)
                .toList();

        PagedModel.PageMetadata pageMetadata = new PagedModel.PageMetadata(
                giftCertificatePage.getSize(),
                giftCertificatePage.getNumber(),
                giftCertificatePage.getTotalElements(),
                giftCertificatePage.getTotalPages()
        );

        PagedModel<EntityModel<GiftCertificate>> pagedModel = PagedModel.of(
                entityModels,
                pageMetadata,
                createPageLinks(tags, page, size, giftCertificatePage)
        );

        return ResponseEntity.ok(pagedModel);
    }

    private EntityModel<GiftCertificate> createGiftCertificateEntityModel(GiftCertificate giftCertificate) {
        EntityModel<GiftCertificate> entityModel = EntityModel.of(giftCertificate);
        entityModel.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).getGiftCertificate(giftCertificate.getId())).withSelfRel());
        return entityModel;
    }

    private List<Link> createPageLinks(List<String> tags, int page, int size, Page<GiftCertificate> giftCertificates) {
        List<Link> links = new ArrayList<>();
        links.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).searchGiftCertificatesByTags(tags, page, size)).withSelfRel());

        if (giftCertificates.hasPrevious()) {
            links.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).searchGiftCertificatesByTags(tags, page - 1, size)).withRel("prev"));
        }

        if (giftCertificates.hasNext()) {
            links.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(GiftCertificateController.class).searchGiftCertificatesByTags(tags, page + 1, size)).withRel("next"));
        }

        return links;
    }

}

