package com.epam.esm.api.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        // return a 401 response
        final int status = HttpServletResponse.SC_UNAUTHORIZED;
        log.info("Client could not be authenticated due to: {} Returning 401 response.", authException.toString());
        log.debug("", authException);

        if (!response.isCommitted()) {
            response.setStatus(status);
            response.setContentType("text/plain");
            response.getWriter().println(String.format("%s Contact the system administrator.", authException.getLocalizedMessage()));
        }
    }
}
