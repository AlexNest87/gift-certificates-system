package com.epam.esm.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {
    @GetMapping("/login")
    public String redirectToIndex() {
        return "forward:/index.html";
    }

    @GetMapping("/certificates")
    public String redirectToCert() {
        return "forward:/index.html";
    }
}
