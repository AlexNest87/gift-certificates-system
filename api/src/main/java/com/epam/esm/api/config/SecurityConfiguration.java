package com.epam.esm.api.config;

import com.epam.esm.api.filter.JwtAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import static com.epam.esm.persistence.model.Permission.*;
import static com.epam.esm.persistence.model.Role.ADMIN;
import static com.epam.esm.persistence.model.Role.MANAGER;
import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableMethodSecurity
public class SecurityConfiguration {

    private static final String MANAGEMENT = "api/v1/management/**";
    private static final String ADMINISTRATION = "/api/v1/admin/**";
    private static final String USER = "/api/v1/user/**";

    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;
    private final LogoutHandler logoutHandler;
    private final CustomAuthenticationSuccessHandler authenticationSuccessHandler;
    private final JwtAuthenticationEntryPoint authenticationEntryPoint;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf(AbstractHttpConfigurer::disable)
                .formLogin(form -> form
                        .loginPage("/login")
                        .defaultSuccessUrl("/certificates")
                        .loginProcessingUrl("/certificates")

                        .successHandler(authenticationSuccessHandler).disable()
                )
                .authorizeHttpRequests(auth -> auth.requestMatchers(
                                        "/api/v1/demo-controller",
                                        "/api/v1/auth/**",
                                        "/v2/api-docs",
                                        "/v3/api-docs",
                                        "/v3/api-docs/**",
                                        "/swagger-resources",
                                        "/swagger-resources/**",
                                        "/configuration/ui",
                                        "/configuration/security",
                                        "/swagger-ui/**",
                                        "/webjars/**",
                                        "/swagger-ui.html",
                                        "/",
                                        "/index.html",
                                        "/login",
                                        "/certificates",
                                        "/login/**",
                                        "/static/**",
                                        "/*.ico",
                                        "/*.json",
                                        "/*.png"
                                )
                                .permitAll()

                                .requestMatchers(MANAGEMENT).hasAnyRole(ADMIN.name(), MANAGER.name())
                                .requestMatchers(GET, MANAGEMENT).hasAnyAuthority(ADMIN_READ.name(), MANAGER_READ.name())
                                .requestMatchers(POST, MANAGEMENT).hasAnyAuthority(ADMIN_CREATE.name(), MANAGER_CREATE.name())
                                .requestMatchers(PUT, MANAGEMENT).hasAnyAuthority(ADMIN_UPDATE.name(), MANAGER_UPDATE.name())
                                .requestMatchers(DELETE, MANAGEMENT).hasAnyAuthority(ADMIN_DELETE.name(), MANAGER_DELETE.name())
                                .requestMatchers(ADMINISTRATION).hasRole(ADMIN.name())
                                .requestMatchers(GET, ADMINISTRATION).hasAuthority(ADMIN_READ.name())
                                .requestMatchers(POST, ADMINISTRATION).hasAuthority(ADMIN_CREATE.name())
                                .requestMatchers(PUT, ADMINISTRATION).hasAuthority(ADMIN_UPDATE.name())
                                .requestMatchers(DELETE, ADMINISTRATION).hasAuthority(ADMIN_DELETE.name())
                                .requestMatchers(GET, USER).hasAuthority(USER_READ.name())
                                .requestMatchers(POST, USER).hasAuthority(USER_CREATE.name())
                                .requestMatchers(PUT, USER).hasAuthority(USER_UPDATE.name())
                                .requestMatchers(PUT, "/api/v1/gift-certificates/**").hasAuthority(ADMIN_UPDATE.name())

                                .anyRequest()
                                .authenticated()

                )
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling((exceptionHandling) ->
                        exceptionHandling
                                .authenticationEntryPoint(authenticationEntryPoint)
                )
                .logout(logout -> logout.logoutUrl("/api/v1/auth/logout"))
                .logout(logout -> logout.addLogoutHandler(logoutHandler))
                .logout(logout -> logout.logoutSuccessHandler(
                        (request, response, authentication) -> SecurityContextHolder.clearContext()));

        return http.build();
    }

}

