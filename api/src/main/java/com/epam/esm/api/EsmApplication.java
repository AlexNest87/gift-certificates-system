package com.epam.esm.api;

import com.epam.esm.persistence.config.DAOModuleConfig;
import com.epam.esm.service.config.ServiceModuleConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({DAOModuleConfig.class, ServiceModuleConfig.class})
public class EsmApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsmApplication.class, args);
	}

}
