package com.epam.esm.api.controller;

import com.epam.esm.persistence.model.Tag;
import com.epam.esm.service.TagService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/tags")
public class TagController {

    private final TagService tagService;

    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @PostMapping
    public ResponseEntity<EntityModel<Tag>> createTag(@RequestBody Tag tag) {
        Tag createdTag = tagService.createTag(tag);
        EntityModel<Tag> entityModel = EntityModel.of(createdTag,
                linkTo(methodOn(TagController.class).getTag(createdTag.getId())).withSelfRel());
        return ResponseEntity.created(URI.create("/tags/" + createdTag.getId())).body(entityModel);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<Tag>> getTag(@PathVariable Long id) {
        Tag tag = tagService.getTagById(id);
        EntityModel<Tag> entityModel = EntityModel.of(tag,
                linkTo(methodOn(TagController.class).getTag(id)).withSelfRel(),
                linkTo(methodOn(TagController.class).getAllTags(0, 10)).withRel("tags"));
        return ResponseEntity.ok(entityModel);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTag(@PathVariable Long id) {
        tagService.deleteTag(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/all")
    public ResponseEntity<CollectionModel<EntityModel<Tag>>> getAllTags(@RequestParam(defaultValue = "0") int page,
                                                                        @RequestParam(defaultValue = "10") int size) {
        List<Tag> tags = tagService.getAllTags(page, size);

        if (tags.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            List<EntityModel<Tag>> entityModels = tags.stream()
                    .map(this::createTagEntityModel)
                    .toList();

            CollectionModel<EntityModel<Tag>> collectionModel = CollectionModel.of(entityModels,
                    linkTo(methodOn(TagController.class).getAllTags(page, size)).withSelfRel());

            return ResponseEntity.ok(collectionModel);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<EntityModel<Tag>> updateGiftCertificate(@PathVariable Long id, @RequestBody Tag tag) {
        tag.setId(id);
        Tag updatedTag = tagService.updateTag(tag);

        EntityModel<Tag> entityModel = EntityModel.of(updatedTag,
                linkTo(methodOn(TagController.class).getTag(updatedTag.getId())).withSelfRel(),
                linkTo(methodOn(TagController.class).getAllTags(0, 10)).withRel("allTags"));
        return ResponseEntity.ok(entityModel);
    }

    private EntityModel<Tag> createTagEntityModel(Tag tag) {
        return EntityModel.of(tag,
                linkTo(methodOn(TagController.class).getTag(tag.getId())).withSelfRel(),
                linkTo(methodOn(TagController.class).getAllTags(0, 10)).withRel("allTags"));
    }
}