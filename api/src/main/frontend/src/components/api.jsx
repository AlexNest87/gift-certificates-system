import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:8080', // Базовый URL сервера
});

// Добавляем токен в заголовки перед отправкой каждого запроса
api.interceptors.request.use(config => {
    const token = localStorage.getItem('jwtToken');
    if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
});

export default api;
