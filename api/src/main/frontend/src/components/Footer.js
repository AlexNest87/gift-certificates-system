import React from 'react';
import '../css/Footer.css';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container">
                <p className="text-center">2023, EXPEDIA Student</p>
            </div>
        </footer>
    );
};

export default Footer;
