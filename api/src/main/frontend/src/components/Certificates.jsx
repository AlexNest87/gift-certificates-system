import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import "../css/Certificates.css";
import CertificatesNavbar from "./CertificatesNavbar";
import {Button, Col, Form, Modal, Row} from "react-bootstrap";
import api from './api';
import { Dropdown, DropdownButton } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";

export default function Certificates({}) {
    const [certificates, setCertificates] = useState([]);
    const [certificateToDelete, setCertificateToDelete] = useState(null);
    const [certificateToEdit, setCertificateToEdit] = useState(null);
    const [title, setTitleToUpdate] = useState('');
    const [description, setDescription] = useState('');
    const [duration, setDuration] = useState('');
    const [price, setPrice] = useState('');
    const [tags, setTags] = useState([]);
    const [newTag, setNewTag] = useState('');
    const [modalOpen, setModalOpen] = useState(false);
    const [filteredCertificates, setFilteredCertificates] = useState([]);
    const [sortDirectionDatetime, setSortDirectionDatetime] = useState("desc");
    const [sortColumnDatetime, setSortColumnDatetime] = useState("lastUpdateDate");
    const [sortDirectionTitle, setSortDirectionTitle] = useState("desc");
    const [sortColumnTitle, setSortColumnTitle] = useState("name");
    const pageSizeOptions = [10, 20, 50];
    const [currentPage, setCurrentPage] = useState(1);
    const [paginatedCertificates, setPaginatedCertificates] = useState([]);
    const [selectedCertificate, setSelectedCertificate] = useState(null);

    const location = useLocation();
    const navigate = useNavigate();

    const searchQueryParam = new URLSearchParams(location.search).get("searchQuery");
    const pageSizeParam = new URLSearchParams(location.search).get("pageSize");
    const pageParam = new URLSearchParams(location.search).get("page");

    const [searchQuery, setSearchQuery] = useState(searchQueryParam || "");
    const [pageSize, setPageSize] = useState(pageSizeParam || 10);
    const [page, setPage] = useState(pageParam || 1);

    useEffect(() => {
        const queryParams = new URLSearchParams();
        if (searchQuery) queryParams.set("searchQuery", searchQuery);
        if (pageSize) queryParams.set("pageSize", pageSize);
        if (page) queryParams.set("page", page);

        navigate(`?${queryParams.toString()}`, { replace: true });
    }, [searchQuery, pageSize, page, navigate]);


    const handleViewCertificate = (certificate) => {
        setSelectedCertificate(certificate);
    };

    const handlePageChange = (page) => {
        setCurrentPage(page);
        localStorage.setItem('currentPage', page.toString());

        const queryParams = new URLSearchParams(location.search);
        queryParams.set("page", page);

        navigate(`?${queryParams.toString()}`, { replace: true });
    };

    const handlePageSizeChange = (newPageSize) => {
        console.log("Changing page size to:", newPageSize);
        setPageSize(newPageSize);
        localStorage.setItem('pageSize', newPageSize.toString());
        loadCertificates(newPageSize);
    };


    const handleSortDatetime = (column) => {
        if (column === sortColumnDatetime) {
            setSortDirectionDatetime(sortDirectionDatetime === "asc" ? "desc" : "asc");
        } else {
            setSortColumnDatetime(column);
            setSortDirectionDatetime("asc");
        }
        loadCertificates();
    };

    const handleSortTitle = (column) => {
        if (column === sortColumnTitle) {
            setSortDirectionTitle(sortDirectionTitle === "asc" ? "desc" : "asc");
        } else {
            setSortColumnTitle(column);
            setSortDirectionTitle("asc");
        }
        loadCertificates();
    };

    const handleTitleUpdate = (event) => {
        setTitleToUpdate(event.target.value);
    };

    const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
    };

    const handleDurationChange = (event) => {
        setDuration(event.target.value);
    };

    const handlePriceChange = (event) => {
        setPrice(event.target.value);
    };

    const handleDeleteTag = (i) => {
        setTags(tags.filter((tag, index) => index !== i));
    };

    const handleAddTag = () => {
        if (newTag.trim() !== '') {
            setTags([...tags, {id: tags.length + 1, text: newTag}]);
            setNewTag('');
        }
    };

    const handleSubmit = () => {
        const tagStrings = tags.map(tag => tag.text);
        const certificateData = {
            name: title !== '' ? title : certificateToEdit.name,
            description: description !== '' ? description : certificateToEdit.description,
            duration: duration !== '' ? duration : certificateToEdit.duration,
            price: price !== '' ? price : certificateToEdit.price,
            tags: tagStrings
        };

        api.put(`/gift-certificates/${certificateToEdit.id}`, certificateData)
            .then(response => {
                closeModal();
            })
            .catch(error => {
                console.error('Error editing certificate:', error);
            });

    };


    const closeModal = () => {
        setModalOpen(false);
        setTitleToUpdate('');
        setDescription('');
        setDuration('');
        setPrice('');
        setTags([]);
        setNewTag('');
        loadCertificates();
        setCertificateToEdit(null);
    };


    useEffect(() => {
        loadCertificates();
    }, []);

    useEffect(() => {
        const startIndex = (currentPage - 1) * pageSize;
        const endIndex = startIndex + pageSize;
        const certificatesToDisplay = filteredCertificates.length > 0 ? filteredCertificates : certificates;
        const newPaginatedCertificates = certificatesToDisplay.slice(startIndex, endIndex);
        setPaginatedCertificates(newPaginatedCertificates);
    }, [filteredCertificates, certificates, pageSize, currentPage]);

    useEffect(() => {
        const savedPage = localStorage.getItem('currentPage');

        if (savedPage !== null) {
            setCurrentPage(parseInt(savedPage));
        }
    }, []);

    useEffect(() => {
        const savedPageSize = localStorage.getItem('pageSize');
        if (savedPageSize !== null) {
            setPageSize(parseInt(savedPageSize));
        }
    }, []);

    const loadCertificates = async () => {
        try {
            const result = await api.get('/gift-certificates/all');
            let sortedCertificates = result.data._embedded.giftCertificateList;

            if (sortColumnDatetime === "lastUpdateDate") {
                sortedCertificates.sort((a, b) => {
                    return sortDirectionDatetime === "asc"
                        ? a.lastUpdateDate.localeCompare(b.lastUpdateDate)
                        : b.lastUpdateDate.localeCompare(a.lastUpdateDate);
                });
            } else if (sortColumnTitle === "name") {
                sortedCertificates.sort((a, b) => {
                    return sortDirectionTitle === "asc"
                        ? a.name.localeCompare(b.name)
                        : b.name.localeCompare(a.name);
                });
            }

            setCertificates(sortedCertificates);
        } catch (error) {
        }
    };

    const deleteCertificate = (id) => {
        const certificate = certificates.find(cert => cert.id === id);
        setCertificateToDelete(certificate);
    };

    const editCertificate = (id) => {
        const certificate = certificates.find(cert => cert.id === id);
        setCertificateToEdit(certificate);
    };


    const confirmDelete = async (id) => {
        try {
            await api.delete(`/gift-certificates/${id}`);
            const updatedCertificates = certificates.filter(cert => cert.id !== id);
            setCertificates(updatedCertificates);
            setCertificateToDelete(null);
        } catch (error) {
            console.error('Error deleting certificate:', error);
        }
    };


    const [errorMessage, setErrorMessage] = useState('');

    const handleShowErrorMessage = (message) => {
        setErrorMessage(message);
    };

    const handleCloseError = () => {
        setErrorMessage('');
    };

    const handleSearchSubmit = () => {
        const filteredCertificates = certificates.filter(certificate => {

            let certificateTagNames = certificate.tags.map(tag => tag.name.toLowerCase());

            const titleTagsDescriptionMatch = (
                certificate.name.includes(searchQuery) ||
                certificate.description.includes(searchQuery) ||
                certificateTagNames.some(tagName => tagName.includes(searchQuery.toLowerCase()))
            );

            return titleTagsDescriptionMatch;
        });
        setFilteredCertificates(filteredCertificates);
    };

    const totalPages = Math.ceil(filteredCertificates.length / pageSize);
    const pageNumbers = Array.from({ length: 9 }, (_, index) => index + 1);

    return (
        <div>
            <CertificatesNavbar
                onCertificatesUpdated={loadCertificates}
                showErrorMessage={handleShowErrorMessage}
            />

            <div className="container">
                <div className={`error-box ${errorMessage ? "" : "hidden"}`}>
                    <div className="error-message">{errorMessage}</div>
                    <span className="close-icon" onClick={handleCloseError}>
          <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              fill="rgba(139, 69, 19, 1)"
              className="bi bi-x"
              viewBox="0 0 16 16"
          >
            <path
                d="M.293 1.293a1 1 0 011.414 0L8 6.586l6.293-6.293a1 1 0 111.414 1.414L9.414 8l6.293 6.293a1 1 0 01-1.414 1.414L8 9.414l-6.293 6.293a1 1 0 01-1.414-1.414L6.586 8 .293 1.707a1 1 0 010-1.414z"
            />
          </svg>
        </span>
                </div>
                <div className="py-4">
                    <div className="table-container">
                        <div className="search-box">
                            <Form>
                                <Row>
                                    <Col sm={12}>
                                        <div className="search-input">
                                            <Form.Control
                                                type="text"
                                                placeholder="Search..."
                                                value={searchQuery}
                                                onChange={(e) => setSearchQuery(e.target.value)}
                                            />
                                            <Button
                                                className="go-btn"
                                                variant="primary"
                                                onClick={handleSearchSubmit}
                                            >
                                                Go!
                                            </Button>
                                        </div>
                                    </Col>
                                </Row>
                            </Form>
                        </div>
                        <table className="table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col" onClick={() => handleSortDatetime("lastUpdateDate")}>
        <span className="sort-icon">
          {sortColumnDatetime === "lastUpdateDate" && (
              <span className={`triangle ${sortDirectionDatetime}`}></span>
          )}
        </span>
                                    Datetime
                                </th>
                                <th scope="col" onClick={() => handleSortTitle("name")}>
        <span className="sort-icon">
          {sortColumnTitle === "name" && (
              <span className={`triangle ${sortDirectionTitle}`}></span>
          )}
        </span>
                                    Title
                                </th>
                                <th scope="col">Tags</th>
                                <th scope="col">Description</th>
                                <th scope="col">Price</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {paginatedCertificates.map((certificate) => (
                                <tr key={certificate.lastUpdateDate}>
                                    <th scope="row">{certificate.lastUpdateDate}</th>
                                    <td>{certificate.name}</td>
                                    <td>
                                        {certificate.tags.map((tag) => (
                                            <span key={tag.id}>{tag.name}, </span>
                                        ))}
                                    </td>
                                    <td>{certificate.description}</td>
                                    <td>{certificate.price}</td>
                                    <td>
                                        <button
                                            className="btn btn-primary mx-2"
                                            onClick={() => handleViewCertificate(certificate)}
                                        >
                                            View
                                        </button>
                                        <Link
                                            className="btn btn-outline-primary btn-warning mx-2"
                                            onClick={() => editCertificate(certificate.id)}
                                        >
                                            Edit
                                        </Link>
                                        <button
                                            className="btn btn-danger mx-2"
                                            onClick={() => deleteCertificate(certificate.id)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div className="page-size-selector">


                <div className="pagination-container">
                    {/*Paging*/}
                    <div className="pagination">
                        {/*First page button*/}
                        <button
                            className={`pagination-button ${currentPage === 1 ? 'disabled' : ''}`}
                            onClick={() => handlePageChange(1)}
                            disabled={currentPage === 1}
                        >
                            &laquo;
                        </button>

                        {/*Pages*/}
                        <div className="page-numbers">
                            {pageNumbers.map(number => (
                                <button
                                    key={number}
                                    className={`page-number-button ${currentPage === number ? 'active' : ''}`}
                                    onClick={() => handlePageChange(number)}
                                    style={{ border: 'none', background: 'white', padding: '5px 10px' }}
                                >
                                    {number}
                                </button>
                            ))}
                        </div>


                        {/*Last Page Button*/}
                        <button
                            className={`pagination-button ${currentPage === totalPages ? 'disabled' : ''}`}
                            onClick={() => handlePageChange(totalPages)}
                            disabled={currentPage === totalPages}
                        >
                            &raquo;
                        </button>
                    </div>

                </div>

                <DropdownButton
                    title={`${pageSize}`}
                    id="page-size-dropdown"
                    variant="light"
                    drop="up"
                >
                    {pageSizeOptions.map((option) => (
                        <Dropdown.Item
                            key={option}
                            active={pageSize === option}
                            onClick={() => handlePageSizeChange(option)}
                        >
                            {option}
                        </Dropdown.Item>
                    ))}
                </DropdownButton>
            </div>
            <Modal show={certificateToDelete !== null} onHide={() => setCertificateToDelete(null)}>
                <Modal.Header closeButton className="custom2-modal-header">
                    <Modal.Title>Delete Confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body className="custom2-modal-body">
                    Do you really want to delete certificate with id = {certificateToDelete?.id}?
                </Modal.Body>
                <Modal.Footer className="custom2-modal-footer">
                    <Button className="btn btn-danger btn-custom" variant="danger"
                            onClick={() => confirmDelete(certificateToDelete?.id)}>
                        Yes
                    </Button>
                    <Button variant="secondary" onClick={() => setCertificateToDelete(null)}>
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={certificateToEdit !== null} onHide={() => setCertificateToEdit(null)} size="lg">
                <Modal.Header className="custom-modal-header">
                    <Modal.Title>Edit Certificate</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="title">
                            <Row>
                                <Form.Label column sm={2}>
                                    Title
                                </Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        defaultValue={certificateToEdit ? certificateToEdit.name : ''}
                                        onChange={handleTitleUpdate}
                                    />
                                </Col>
                            </Row>
                        </Form.Group>
                        <Form.Group controlId="description">
                            <Row>
                                <Form.Label column sm={2}>
                                    Description
                                </Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        as="textarea"
                                        rows={3}
                                        defaultValue={certificateToEdit ? certificateToEdit.description : ''}
                                        onChange={handleDescriptionChange}
                                        className="w-100"
                                        style={{display: 'inline-block', minHeight: '150px'}}
                                    />
                                </Col>
                            </Row>
                        </Form.Group>
                        <Form.Group controlId="Duration">
                            <Row>
                                <Form.Label column sm={2}>
                                    Duration
                                </Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        defaultValue={certificateToEdit ? certificateToEdit.duration : ''}
                                        onChange={handleDurationChange}
                                        style={{display: 'inline-block'}}
                                    />
                                </Col>
                            </Row>
                        </Form.Group>
                        <Form.Group controlId="Duration">
                            <Row>
                                <Form.Label column sm={2}>
                                    Price
                                </Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        defaultValue={certificateToEdit ? certificateToEdit.price : ''}
                                        onChange={handlePriceChange}
                                        style={{display: 'inline-block'}}
                                    />
                                </Col>
                            </Row>
                        </Form.Group>
                        <Form.Group controlId="tags">
                            <Row className="align-items-center">
                                <Col sm={2}>
                                    <Form.Label>Tags</Form.Label>
                                </Col>
                                <Col sm={9}>
                                    <div className="input-group align-items-stretch">
                                        <Form.Control
                                            type="text"
                                            value={newTag}
                                            onChange={(e) => setNewTag(e.target.value)}
                                            style={{display: 'inline-block'}}
                                        />
                                        <div className="input-group-append">
                                            <Button variant="primary" size="lg" onClick={handleAddTag}
                                                    className="h-75">
                                                Add
                                            </Button>
                                        </div>
                                    </div>
                                    <div>
                                        {tags.map((tag, index) => (
                                            <span key={tag.id} className="tag">
                                                {tag.text}
                                                <span className="delete-tag" onClick={() => handleDeleteTag(index)}>
                                                    &times;
                                                </span>
                                            </span>
                                        ))}
                                    </div>
                                </Col>
                            </Row>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-center">
                    <Button variant="primary" onClick={handleSubmit}>
                        Edit
                    </Button>
                    <Button variant="secondary" className="custom-button" onClick={() => setCertificateToEdit(null)}>
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>

            {/* Modal window for displaying certificate information */}
            <Modal show={selectedCertificate !== null} onHide={() => setSelectedCertificate(null)}>
                <Modal.Header closeButton className="custom2-modal-header">
                    <Modal.Title>Certificate Details</Modal.Title>
                </Modal.Header>
                <Modal.Body className="custom2-modal-body">
                    {selectedCertificate && (
                        <div>
                            <h5>Title: {selectedCertificate.name}</h5>
                            <p>Description: {selectedCertificate.description}</p>
                            <p>Duration: {selectedCertificate.duration}</p>
                            <p>Price: {selectedCertificate.price}</p>
                            <p>Tags: {selectedCertificate.tags.map((tag) => tag.name).join(", ")}</p>
                        </div>
                    )}
                </Modal.Body>
                <Modal.Footer className="custom2-modal-footer">
                    <Button variant="secondary" onClick={() => setSelectedCertificate(null)}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}
