import '../App.css';
import LoginForm from "./LoginForm";
import Navbar from "./Navbar";
import Footer from "./Footer";


function Login() {

    return (
        <div>
            <Navbar />
            <LoginForm/>
            <Footer />
        </div>
    );
}

export default Login;
