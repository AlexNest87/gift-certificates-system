import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import '../css/Navbar.css';
import {Button, Col, Form, Modal, Row} from 'react-bootstrap';
import api from './api';

const CertificatesNavbar = ({onCertificatesUpdated, showErrorMessage}) => {
    const [modalOpen, setModalOpen] = useState(false);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [duration, setDuration] = useState('');
    const [price, setPrice] = useState('');
    const [tags, setTags] = useState([]);
    const [newTag, setNewTag] = useState('');
    const [modalErrorMessage, setModalErrorMessage] = useState('');


    const handleAddTag = () => {
        if (newTag.trim() !== '') {
            setTags([...tags, {id: tags.length + 1, text: newTag}]);
            setNewTag('');
        }
    };

    const openModal = () => {
        setModalOpen(true);
    };

    const closeModal = () => {
        setModalOpen(false);
        setTitle('');
        setDescription('');
        setDuration('');
        setPrice('');
        setTags([]);
        setNewTag('');
    };

    const validateFields = () => {
        let error = '';

        if (title.trim() === '') {
            error = 'Title is required';
        } else if (title.length < 6 || title.length > 30) {
            error = 'Title should be between 6 and 30 characters';
        }

        if (description.trim() === '') {
            error = 'Description is required';
        } else if (description.length < 12 || description.length > 1000) {
            error = 'Description should be between 12 and 1000 characters';
        }

        if (price.trim() === '') {
            error = 'Price is required';
        }

        return error;
    };

    const handleTitleChange = (event) => {
        setTitle(event.target.value);
    };

    const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
    };

    const handleDurationChange = (event) => {
        setDuration(event.target.value);
    };

    const handlePriceChange = (event) => {
        setPrice(event.target.value);
    };

    const handleDeleteTag = (i) => {
        setTags(tags.filter((tag, index) => index !== i));
    };

    const handleSubmit = () => {
        const tagStrings = tags.map(tag => tag.text);
        const certificateData = {
            name: title,
            description: description,
            duration: duration,
            price: price,
            tags: tagStrings
        };

        if (validateFields() == null || validateFields() == '') {
            api.post('/gift-certificates', certificateData)
                .then(response => {
                    console.log('Certificate created:', response.data);
                    closeModal();
                })
                .catch(error => {
                    console.error('Error creating certificate:', error);
                });
        }

        console.log('Title:', title);
        console.log('Description:', description);
        console.log('modalErrorMessage:', modalErrorMessage);
        closeModal();
        setTimeout(() => {
            if (validateFields() != null && validateFields() != '') {
                showErrorMessage(validateFields());
            }
            onCertificatesUpdated();
        }, 500);
    };

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">
                    Admin UI
                </a>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <button
                                className="btn btn-primary mx-2"
                                onClick={openModal}
                            >
                                Add new
                            </button>
                        </li>
                        <li className="nav-item">
                        <span className="nav-link">
                          {localStorage.getItem('username')}
                        </span>
                        </li>
                        <li className="nav-item">
                            <Link to="/login" className="nav-link">
                                Logout
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
            <Modal show={modalOpen} onHide={closeModal} size="lg">
                <Modal.Header className="custom-modal-header">
                    <Modal.Title>Add new Certificate</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="title">
                            <Row>
                                <Form.Label column sm={2}>
                                    Title
                                </Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        value={title}
                                        onChange={handleTitleChange}
                                    />
                                </Col>
                            </Row>
                        </Form.Group>
                        <Form.Group controlId="description">
                            <Row>
                                <Form.Label column sm={2}>
                                    Description
                                </Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        as="textarea"
                                        rows={3}
                                        value={description}
                                        onChange={handleDescriptionChange}
                                        className="w-100"
                                        style={{display: 'inline-block', minHeight: '150px'}}
                                    />
                                </Col>
                            </Row>
                        </Form.Group>
                        <Form.Group controlId="Duration">
                            <Row>
                                <Form.Label column sm={2}>
                                    Duration
                                </Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        value={duration}
                                        onChange={handleDurationChange}
                                        style={{display: 'inline-block'}}
                                    />
                                </Col>
                            </Row>
                        </Form.Group>
                        <Form.Group controlId="Duration">
                            <Row>
                                <Form.Label column sm={2}>
                                    Price
                                </Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        value={price}
                                        onChange={handlePriceChange}
                                        style={{display: 'inline-block'}}
                                    />
                                </Col>
                            </Row>
                        </Form.Group>
                        <Form.Group controlId="tags">
                            <Row className="align-items-center">
                                <Col sm={2}>
                                    <Form.Label>Tags</Form.Label>
                                </Col>
                                <Col sm={9}>
                                    <div className="input-group align-items-stretch">
                                        <Form.Control
                                            type="text"
                                            value={newTag}
                                            onChange={(e) => setNewTag(e.target.value)}
                                            style={{display: 'inline-block'}}
                                        />
                                        <div className="input-group-append">
                                            <Button variant="primary" size="lg" onClick={handleAddTag} className="h-75">
                                                Add
                                            </Button>
                                        </div>
                                    </div>
                                    <div>
                                        {tags.map((tag, index) => (
                                            <span key={tag.id} className="tag">
                                                {tag.text}
                                                <span className="delete-tag" onClick={() => handleDeleteTag(index)}>
                                                    &times;
                                                </span>
                                            </span>
                                        ))}
                                    </div>
                                </Col>
                            </Row>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-center">
                    {modalErrorMessage && (
                        <div className="error-line">{modalErrorMessage}</div>
                    )}
                    <Button variant="primary" onClick={handleSubmit}>
                        Save
                    </Button>
                    <Button variant="secondary" className="custom-button" onClick={closeModal}>
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>
        </nav>
    );
};

export default CertificatesNavbar;
