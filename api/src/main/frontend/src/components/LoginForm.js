import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/LoginForm.css';
import axios from 'axios';

import {useState,} from 'react';
import {useNavigate} from 'react-router-dom';

const LoginForm = ({onSubmit}) => {
    const [loginValue, setLoginValue] = useState('');
    const [passwordValue, setPasswordValue] = useState('');
    const [loginError, setLoginError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();

    const validateLogin = (value) => {
        if (value.length < 3 || value.length > 30) {
            return 'Login must be between 3 and 30 characters';
        }
        return '';
    };

    const validatePassword = (value) => {
        if (value.length < 4 || value.length > 30) {
            return 'Password must be between 4 and 30 characters';
        }
        return '';
    };

    const handleLoginChange = (e) => {
        const value = e.target.value;
        setLoginValue(value);
        setLoginError(validateLogin(value));
    };

    const handlePasswordChange = (e) => {
        const value = e.target.value;
        setPasswordValue(value);
        setPasswordError(validatePassword(value));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        if (loginValue === '' || passwordValue === '') {
            setErrorMessage('Please fill in all fields.');
        } else {
            const apiUrl = 'http://localhost:8080/api/v1/auth/authenticate';

            const requestBody = {
                email: loginValue,
                password: passwordValue
            };

            console.log('Sending request with data:', requestBody);

            try {
                const response = await axios.post(apiUrl, requestBody);
                console.log('Response from server:', response);

                if (response.status === 200) {
                    let token = response.data.access_token;
                    let username = response.data.username;
                    localStorage.setItem("jwtToken", token)
                    localStorage.setItem("username", username)
                    console.log('Successful authorization');
                    navigate('/certificates');
                } else {
                    // Error Handling
                    console.error('Authorization error');
                }
            } catch (error) {
                // Network error handling
                if (error.response && error.response.data) {
                    setErrorMessage(error.response.data);
                } else {
                    setErrorMessage('Authorization error');
                }
            }
        }
    };

    return (
        <div className="login-container">
            <div className="login-box">
                <h2 className="login-title">Login</h2>
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Username"
                            value={loginValue}
                            onChange={handleLoginChange}
                        />
                        {loginError && <div className="text-danger mt-2">{loginError}</div>}
                    </div>
                    <div className="mb-3">
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Password"
                            value={passwordValue}
                            onChange={handlePasswordChange}
                        />
                        {passwordError && (
                            <div className="text-danger mt-2">{passwordError}</div>
                        )}
                    </div>
                    {errorMessage && (
                        <div className="text-danger mt-2">{errorMessage}</div>
                    )}
                    <div className="login-button-container">
                        <button type="submit" className="btn btn-primary">
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default LoginForm;