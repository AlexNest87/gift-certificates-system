import './App.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Login from "./components/Login";
import Certificates from "./components/Certificates";


function App() {

  return (
      <div>
     <BrowserRouter>
         <Routes>
             <Route path="/login" element={<Login/>} />
             <Route path="/certificates" element= {<Certificates/>} />
         </Routes>
     </BrowserRouter>
      </div>
  );
}

export default App;
