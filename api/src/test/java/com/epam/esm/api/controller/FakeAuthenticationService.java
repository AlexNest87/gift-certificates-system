package com.epam.esm.api.controller;

import com.epam.esm.api.auth.AuthenticationRequest;
import com.epam.esm.api.auth.AuthenticationResponse;
import com.epam.esm.api.auth.AuthenticationService;
import com.epam.esm.api.auth.RegisterRequest;
import com.epam.esm.persistence.repository.TokenRepository;
import com.epam.esm.persistence.repository.UserRepository;
import com.epam.esm.service.JwtService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

public class FakeAuthenticationService extends AuthenticationService {
    public FakeAuthenticationService(UserRepository repository, TokenRepository tokenRepository, PasswordEncoder passwordEncoder, JwtService jwtService, AuthenticationManager authenticationManager) {
        super(repository, tokenRepository, passwordEncoder, jwtService, authenticationManager);
    }

    @Override
    public AuthenticationResponse register(RegisterRequest request) {
        // Вернуть фейковый ответ для метода register
        return new AuthenticationResponse("fakeAccessToken", "fakeRefreshToken");
    }

    @Override
    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        // Вернуть фейковый ответ для метода authenticate
        return new AuthenticationResponse("fakeAccessToken", "fakeRefreshToken");
    }

    @Override
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) {
        // Реализовать фейковый метод refreshToken
        // Может быть пустым или просто вернуть HTTP 200 OK
        // В данном случае, фейковая реализация просто вернет HTTP 200 OK
        response.setStatus(HttpServletResponse.SC_OK);
    }
}
