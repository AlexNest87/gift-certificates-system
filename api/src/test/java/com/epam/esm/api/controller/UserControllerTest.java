package com.epam.esm.api.controller;

import com.epam.esm.persistence.model.GiftCertificate;
import com.epam.esm.persistence.model.Order;
import com.epam.esm.persistence.model.OrderDTO;
import com.epam.esm.persistence.model.User;
import com.epam.esm.service.UserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;


    @MockBean
    private FakeAuthenticationService authenticationService;

    @MockBean
    private UserDetailsService userDetailsService;

    @Test
    @WithMockUser(username = "user1", roles = "ADMIN")
    void testGetAllUsers() throws Exception {
        int page = 0;
        int size = 10;

        Page<User> userPage = new PageImpl<>(Arrays.asList(
                new User(1, "John"),
                new User(2, "Jane")
        ));
        when(userService.getAllUsers(any(Pageable.class))).thenReturn(userPage);

        mockMvc.perform(get("/users/all")
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.userDTOList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.userDTOList[0].id", is(1)))
                .andExpect(jsonPath("$._embedded.userDTOList[0].firstName", is("John")))
                .andExpect(jsonPath("$._embedded.userDTOList[1].id", is(2)))
                .andExpect(jsonPath("$._embedded.userDTOList[1].firstName", is("Jane")));

        verify(userService, times(1)).getAllUsers(any(Pageable.class));
    }


    @Test
    @WithMockUser(username = "user2", roles = "ADMIN")
    void testGetUserById_UserExists() throws Exception {
        Integer userId = 1;
        User user = new User(userId, "John");
        when(userService.getUserById(userId)).thenReturn(user);

        mockMvc.perform(get("/users/{id}", userId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.firstName", is("John")));

        verify(userService, times(1)).getUserById(userId);
    }

    @Test
    @WithMockUser(username = "user3", roles = "ADMIN")
    void testGetUserById_UserDoesNotExist() throws Exception {
        Integer userId = 1;
        when(userService.getUserById(userId)).thenReturn(null);

        mockMvc.perform(get("/users/{id}", userId))
                .andExpect(status().isNotFound());

        verify(userService, times(1)).getUserById(userId);
    }

//    @Test
    @WithMockUser(username = "user4", roles = "ADMIN")
    void testPurchaseGiftCertificate() throws Exception {
        Integer userId = 1;
        Long certificateId = 1L;
        Order order = new Order();
        order.setId(1);
        order.setUser(new User(userId));
        order.setGiftCertificate(new GiftCertificate(certificateId));
        when(userService.getUserById(userId)).thenReturn(new User(userId));
        when(userService.purchaseGiftCertificate(userId, certificateId)).thenReturn(order);

        mockMvc.perform(MockMvcRequestBuilders.post("/users/{userId}/orders/{certificateId}", userId, certificateId))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.user.id", Matchers.equalTo(userId.intValue())))
                .andExpect(jsonPath("$.giftCertificate.id", Matchers.equalTo(certificateId.intValue())));

        verify(userService, Mockito.times(1)).getUserById(userId);
        verify(userService, Mockito.times(1)).purchaseGiftCertificate(userId, certificateId);
    }

    @Test
    @WithMockUser(username = "user6", roles = "ADMIN")
    void testGetUserOrders() throws Exception {
        Integer userId = 1;
        int page = 0;
        int size = 10;

        List<Order> orders = new ArrayList<>();
        orders.add(new Order(1, new User(userId), new GiftCertificate(1L, "Order 1", "GiftCertificate description")));
        orders.add(new Order(2, new User(userId), new GiftCertificate(1L, "Order 2", "GiftCertificate description")));

        Pageable pageable = PageRequest.of(page, size);

        when(userService.getUserById(userId)).thenReturn(new User(userId));
        when(userService.getUserOrders(userId, pageable)).thenReturn(orders);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/{userId}/orders", userId)
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].user.id").value(userId.intValue()))
                .andExpect(jsonPath("$[0].giftCertificateId").value(1L))
                .andExpect(jsonPath("$[0].giftCertificateName").value("Order 1"))
                .andExpect(jsonPath("$[0].giftCertificateDescription").value("GiftCertificate description"))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].user.id").value(userId.intValue()))
                .andExpect(jsonPath("$[1].giftCertificateId").value(1L))
                .andExpect(jsonPath("$[1].giftCertificateName").value("Order 2"))
                .andExpect(jsonPath("$[1].giftCertificateDescription").value("GiftCertificate description"));

        verify(userService, times(1)).getUserOrders(eq(userId), any(Pageable.class));
    }


    @Test
    @WithMockUser(username = "user7", roles = "ADMIN")
    void testGetUserOrderInfo() throws Exception {
        Integer userId = 1;
        int page = 0;
        int size = 10;

        List<Order> orders = new ArrayList<>();
        orders.add(new Order(1, new User(userId), new GiftCertificate(1L, "Order 1",
                "GiftCertificate description", BigDecimal.TEN, 7)));
        orders.add(new Order(2, new User(userId), new GiftCertificate(2L, "Order 2",
                "GiftCertificate description", BigDecimal.ONE, 7)));

        Pageable pageable = PageRequest.of(page, size);

        when(userService.getUserById(userId)).thenReturn(new User(userId));
        when(userService.getUserOrders(userId, pageable)).thenReturn(orders);

        mockMvc.perform(get("/users/{userId}/order-info", userId)
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].orderCost", is(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].orderCost", is(1)));

        verify(userService, times(1)).getUserOrders(userId, pageable);
    }

}
