package com.epam.esm.api.controller;

import com.epam.esm.persistence.model.Tag;
import com.epam.esm.service.TagService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TagController.class)
class TagControllerTest {

    @MockBean
    private TagService tagService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FakeAuthenticationService authenticationService;

    @MockBean
    private UserDetailsService userDetailsService;

    @Test
    @WithMockUser(username = "user1", roles = "ADMIN")
    void testCreateTag() throws Exception {
        Tag tag = new Tag("test tag");
        when(tagService.createTag(Mockito.any(Tag.class))).thenReturn(tag);

        mockMvc.perform(MockMvcRequestBuilders.post("/tags")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"test tag\"}")
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("test tag"));
    }

    @Test
    @WithMockUser(username = "user2", roles = "ADMIN")
    void testGetTag() throws Exception {
        Tag tag = new Tag(1L, "test tag");
        when(tagService.getTagById(1L)).thenReturn(tag);

        mockMvc.perform(MockMvcRequestBuilders.get("/tags/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").value("test tag"));
    }

    @Test
    @WithMockUser(username = "user3", roles = "ADMIN")
    void testDeleteTag() throws Exception {
        Long tagId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/tags/" + tagId)
                        .with(csrf()))
                .andExpect(status().isNoContent());
        verify(tagService, times(1)).deleteTag(tagId);
    }

    @Test
    @WithMockUser(username = "user4", roles = "ADMIN")
    void testGetAllTags() throws Exception {
        List<Tag> tags = Arrays.asList(new Tag(1L, "tag1"), new Tag(2L, "tag2"));
        when(tagService.getAllTags(anyInt(), anyInt())).thenReturn(tags);

        mockMvc.perform(MockMvcRequestBuilders.get("/tags/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.tagList").exists())
                .andExpect(jsonPath("$._embedded.tagList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.tagList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.tagList[0].name").value("tag1"))
                .andExpect(jsonPath("$._embedded.tagList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.tagList[1].name").value("tag2"))
                .andExpect(jsonPath("$._links.self").exists());

        verify(tagService, times(1)).getAllTags(anyInt(), anyInt());
    }

    @Test
    @WithMockUser(username = "user5", roles = "ADMIN")
    void testUpdateTag() throws Exception {
        Tag tag = new Tag(1L, "updated tag");
        when(tagService.updateTag(Mockito.any(Tag.class))).thenReturn(tag);

        mockMvc.perform(MockMvcRequestBuilders.put("/tags/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"updated tag\"}")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").value("updated tag"));
    }
}