package com.epam.esm.api.controller;

import com.epam.esm.persistence.model.GiftCertificate;
import com.epam.esm.persistence.model.Tag;
import com.epam.esm.service.GiftCertificateService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(GiftCertificateController.class)
class GiftCertificateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GiftCertificateService giftCertificateService;

    @MockBean
    private FakeAuthenticationService authenticationService;

    @MockBean
    private UserDetailsService userDetailsService;

    @Test
    @WithMockUser(username = "user1", roles = "ADMIN")
    void testCreateGiftCertificate() throws Exception {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("Test Certificate");
        giftCertificate.setDescription("This is a test certificate");

        GiftCertificate createdGiftCertificate = new GiftCertificate();
        createdGiftCertificate.setId(1L);
        createdGiftCertificate.setName("Test Certificate");
        createdGiftCertificate.setDescription("This is a test certificate");

        when(giftCertificateService.createGiftCertificate(Mockito.any(GiftCertificate.class)))
                .thenReturn(createdGiftCertificate);

        mockMvc.perform(MockMvcRequestBuilders.post("/gift-certificates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(giftCertificate))
                        .with(csrf())) // Добавляем CSRF токен в запрос
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("Test Certificate"))
                .andExpect(jsonPath("$.description").value("This is a test certificate"))
                .andExpect(jsonPath("$._links.create").exists())
                .andExpect(jsonPath("$._links.update").exists())
                .andExpect(jsonPath("$._links.delete").exists());
    }

    @Test
    @WithMockUser(username = "user2", roles = "ADMIN")
    void testGetGiftCertificateById() throws Exception {
        Long certificateId = 1L;

        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setId(certificateId);
        giftCertificate.setName("Test Certificate");
        giftCertificate.setDescription("This is a test certificate");

        when(giftCertificateService.getGiftCertificateById(certificateId))
                .thenReturn(giftCertificate);

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/" + certificateId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(certificateId))
                .andExpect(jsonPath("$.name").value("Test Certificate"))
                .andExpect(jsonPath("$.description").value("This is a test certificate"));
    }

    @Test
    @WithMockUser(username = "user3", roles = "ADMIN")
    void testGetGiftCertificates() throws Exception {
        int page = 0;
        int size = 10;

        GiftCertificate giftCertificate1 = new GiftCertificate();
        giftCertificate1.setId(1L);
        giftCertificate1.setName("Test Certificate 1");
        giftCertificate1.setDescription("This is a test certificate");

        GiftCertificate giftCertificate2 = new GiftCertificate();
        giftCertificate2.setId(2L);
        giftCertificate2.setName("Test Certificate 2");
        giftCertificate2.setDescription("This is another test certificate");

        List<GiftCertificate> giftCertificates = Arrays.asList(giftCertificate1, giftCertificate2);

        when(giftCertificateService.getAllGiftCertificates(page, size)).thenReturn(giftCertificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/all")
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].name").value("Test Certificate 1"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].description").value("This is a test certificate"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].name").value("Test Certificate 2"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].description").value("This is another test certificate"))
                .andExpect(jsonPath("$._links.self").exists());
    }

    @Test
    @WithMockUser(username = "user4", roles = "ADMIN")
    void testUpdateGiftCertificate() throws Exception {
        Long certificateId = 1L;

        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setId(certificateId);
        giftCertificate.setName("Test Certificate");
        giftCertificate.setDescription("This is a test certificate");

        when(giftCertificateService.updateGiftCertificate(Mockito.any(GiftCertificate.class)))
                .thenReturn(giftCertificate);

        mockMvc.perform(MockMvcRequestBuilders.put("/gift-certificates/" + certificateId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(giftCertificate))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(certificateId))
                .andExpect(jsonPath("$.name").value("Test Certificate"))
                .andExpect(jsonPath("$.description").value("This is a test certificate"));
    }

    @Test
    @WithMockUser(username = "user5", roles = "ADMIN")
    void testDeleteGiftCertificate() throws Exception {
        Long certificateId = 1L;

        when(giftCertificateService.deleteGiftCertificateById(Mockito.anyLong())).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.delete("/gift-certificates/" + certificateId)
                        .with(csrf())) // Добавляем CSRF токен в запрос
                .andExpect(status().isNoContent());

        verify(giftCertificateService, Mockito.times(1)).deleteGiftCertificateById(certificateId);
    }

    @Test
    @WithMockUser(username = "user6", roles = "ADMIN")
    void testGetGiftCertificatesByTagName() throws Exception {
        String tagName = "tag1";

        GiftCertificate giftCertificate1 = new GiftCertificate();
        giftCertificate1.setId(1L);
        giftCertificate1.setName("Test Certificate 1");
        giftCertificate1.setDescription("This is a test certificate");
        giftCertificate1.setTags(new HashSet<>(List.of(new Tag("tag1"))));

        GiftCertificate giftCertificate2 = new GiftCertificate();
        giftCertificate2.setId(2L);
        giftCertificate2.setName("Test Certificate 2");
        giftCertificate2.setDescription("This is another test certificate");
        giftCertificate2.setTags(new HashSet<>(List.of(new Tag("tag1"), new Tag("tag2"))));

        List<GiftCertificate> giftCertificates = Arrays.asList(giftCertificate1, giftCertificate2);

        when(giftCertificateService.getAllGiftCertificates(tagName, null, null, 0, 10)).thenReturn(giftCertificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/sorted")
                        .param("tag", tagName)
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].name").value("Test Certificate 1"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].description").value("This is a test certificate"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].name").value("Test Certificate 2"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].description").value("This is another test certificate"))
                .andExpect(jsonPath("$._links.self").exists());
    }

    @Test
    @WithMockUser(username = "user7", roles = "ADMIN")
    void testGetAllGiftCertificatesWithoutParams() throws Exception {
        when(giftCertificateService.getAllGiftCertificates(null, null, null, 0, 10))
                .thenReturn(Collections.emptyList());

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/sorted")
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(username = "user8", roles = "ADMIN")
    void testGetAllGiftCertificatesWithTagName() throws Exception {
        String tagName = "tag1";
        int page = 0;
        int size = 10;

        List<GiftCertificate> giftCertificates = List.of(
                new GiftCertificate(1L, "Test Certificate 1", "This is a test certificate", new HashSet<>(List.of(new Tag("tag1")))),
                new GiftCertificate(2L, "Test Certificate 2", "This is another test certificate", new HashSet<>(List.of(new Tag("tag1"), new Tag("tag2"))))
        );

        when(giftCertificateService.getAllGiftCertificates(tagName, null, null, page, size)).thenReturn(giftCertificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/sorted")
                        .param("tag", tagName)
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].name").value("Test Certificate 1"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].description").value("This is a test certificate"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].name").value("Test Certificate 2"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].description").value("This is another test certificate"))
                .andExpect(jsonPath("$._links.self").exists());
    }


    @Test
    @WithMockUser(username = "user9", roles = "ADMIN")
    void testGetAllGiftCertificatesWithPartOfNameOrDescription() throws Exception {
        String partOfNameOrDescription = "test";
        int page = 0;
        int size = 10;

        List<GiftCertificate> giftCertificates = Arrays.asList(
                new GiftCertificate(1L, "Test Certificate 1", "This is a test certificate", new HashSet<>(Arrays.asList(new Tag("tag1")))),
                new GiftCertificate(2L, "Test Certificate 2", "This is another test certificate", new HashSet<>(Arrays.asList(new Tag("tag1"), new Tag("tag2"))))
        );

        when(giftCertificateService.getAllGiftCertificates(null, partOfNameOrDescription, null, page, size)).thenReturn(giftCertificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/sorted")
                        .param("partOfNameOrDescription", partOfNameOrDescription)
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].name").value("Test Certificate 1"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].description").value("This is a test certificate"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].name").value("Test Certificate 2"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].description").value("This is another test certificate"))
                .andExpect(jsonPath("$._links.self").exists());
    }

    @Test
    @WithMockUser(username = "user10", roles = "ADMIN")
    void testGetAllGiftCertificatesWithSortBy() throws Exception {
        String sortBy = "name";
        int page = 0;
        int size = 10;

        List<GiftCertificate> giftCertificates = Arrays.asList(
                new GiftCertificate(1L, "Test Certificate 1", "This is a test certificate", new HashSet<>(Arrays.asList(new Tag("tag1")))),
                new GiftCertificate(2L, "Test Certificate 2", "This is another test certificate", new HashSet<>(Arrays.asList(new Tag("tag1"))))
        );

        when(giftCertificateService.getAllGiftCertificates(null, null, sortBy, page, size)).thenReturn(giftCertificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/sorted")
                        .param("sortBy", sortBy)
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].name").value("Test Certificate 1"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].description").value("This is a test certificate"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].name").value("Test Certificate 2"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].description").value("This is another test certificate"))
                .andExpect(jsonPath("$._links.self").exists());
    }

    @Test
    @WithMockUser(username = "user11", roles = "ADMIN")
    void testGetAllGiftCertificatesWithAllParams() throws Exception {
        String tagName = "tag1";
        String partOfNameOrDescription = "test";
        String sortBy = "name";
        int page = 0;
        int size = 10;

        List<GiftCertificate> giftCertificates = Arrays.asList(
                new GiftCertificate(1L, "Test Certificate 1", "This is a test certificate", new HashSet<>(Arrays.asList(new Tag("tag1")))),
                new GiftCertificate(2L, "Test Certificate 2", "This is another test certificate", new HashSet<>(Arrays.asList(new Tag("tag1"), new Tag("tag2"))))
        );

        when(giftCertificateService.getAllGiftCertificates(tagName, partOfNameOrDescription, sortBy, page, size)).thenReturn(giftCertificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/sorted")
                        .param("tag", tagName)
                        .param("partOfNameOrDescription", partOfNameOrDescription)
                        .param("sortBy", sortBy)
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].name").value("Test Certificate 1"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].description").value("This is a test certificate"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].name").value("Test Certificate 2"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].description").value("This is another test certificate"))
                .andExpect(jsonPath("$._links.self").exists());
    }

    @Test
    @WithMockUser(username = "user12", roles = "ADMIN")
    void testGetAllGiftCertificatesWithTagAndPartOfNameOrDescription() throws Exception {
        String tagName = "tag1";
        String partOfNameOrDescription = "test";
        int page = 0;
        int size = 10;

        List<GiftCertificate> giftCertificates = Arrays.asList(
                new GiftCertificate(1L, "Test Certificate 1", "This is a test certificate", new HashSet<>(Arrays.asList(new Tag("tag1")))),
                new GiftCertificate(2L, "Test Certificate 2", "This is another test certificate", new HashSet<>(Arrays.asList(new Tag("tag1"), new Tag("tag2"))))
        );

        when(giftCertificateService.getAllGiftCertificates(tagName, partOfNameOrDescription, null, page, size)).thenReturn(giftCertificates);

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/sorted")
                        .param("tag", tagName)
                        .param("partOfNameOrDescription", partOfNameOrDescription)
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].name").value("Test Certificate 1"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].description").value("This is a test certificate"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].name").value("Test Certificate 2"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].description").value("This is another test certificate"))
                .andExpect(jsonPath("$._links.self").exists());
    }

    @Test
    @WithMockUser(username = "user13", roles = "ADMIN")
    void testGetAllTags() throws Exception {
        int page = 0;
        int size = 10;

        GiftCertificate certificate1 = new GiftCertificate(1L, "Test Certificate 1", "This is a test certificate", new HashSet<>(Arrays.asList(new Tag("tag1"))));
        GiftCertificate certificate2 = new GiftCertificate(2L, "Test Certificate 2", "This is another test certificate", new HashSet<>(Arrays.asList(new Tag("tag1"), new Tag("tag2"))));
        List<GiftCertificate> certificates = Arrays.asList(certificate1, certificate2);

        Page<GiftCertificate> certificatePage = new PageImpl<>(certificates);

        when(giftCertificateService.getAllGiftCertificates(page, size)).thenReturn(certificatePage.getContent());

        mockMvc.perform(MockMvcRequestBuilders.get("/gift-certificates/all")
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList").exists())
                .andExpect(jsonPath("$._embedded.giftCertificateList", hasSize(2)))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].id").value(1L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].name").value("Test Certificate 1"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[0].description").value("This is a test certificate"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].id").value(2L))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].name").value("Test Certificate 2"))
                .andExpect(jsonPath("$._embedded.giftCertificateList[1].description").value("This is another test certificate"))
                .andExpect(jsonPath("$._links.self").exists());
    }

}